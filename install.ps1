﻿# Get Admin
param([switch]$Elevated)

function Test-Admin {
    $currentUser = New-Object Security.Principal.WindowsPrincipal $([Security.Principal.WindowsIdentity]::GetCurrent())
    $currentUser.IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)
}
if ((Test-Admin) -eq $false)  {
    if ($elevated) {
        # tried to elevate, did not work, aborting
    } else {
        Start-Process powershell.exe -Verb RunAs -ArgumentList ('-noprofile -noexit -file "{0}" -elevated' -f ($myinvocation.MyCommand.Definition))
    }
    exit
}

# Echo to UTF-8
chcp 65001

# Install Chocolatey
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
$env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine")

# Install Python
choco install python -y
$env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine")

# Install Modules
pip install gpiozero;pip install pigpio;pip install pythonping;pip install paramiko;pip install psutil;pip install pywin32;pip install keyboard;pip install pyvda;pip install click

# Write Python Setup Script
$PythonSetupScript = @"
import os

from pythonping import ping
import ipaddress
import paramiko

PORT = 22
USERNAME = "pi"
PASSWORD = "raspberry"
REMOTESCRIPT = r"""sudo apt-get update -y
sudo apt-get install python3 -y
sudo apt-get install python3-gpiozero -y
sudo apt-get install pigpio -y
sudo raspi-config nonint do_rgpio 1
sudo sed  '/fi/a sudo pigpiod'  /etc/rc.local -i
sudo pigpiod"""

def set_cwd_to_script_location():
    abspath = os.path.abspath(__file__)
    dname = os.path.dirname(abspath)
    os.chdir(dname)

def find_RIP():
    x = ping('raspberrypi')
    rip = x._responses[0].message.source
    rip_version = ipaddress.ip_address(rip).version
    if rip_version != 4:
        raise Exception
    return rip

def main():
    set_cwd_to_script_location()
    IP = find_RIP()
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(IP, PORT, USERNAME, PASSWORD)
    for i in REMOTESCRIPT.splitlines():
        stdin, stdout, stderr = ssh.exec_command(i)
        for line in stdout.readlines():
            print(line)
        for line in stderr.readlines():
            print(line)
    ssh.close()

if __name__ =='__main__':
    main()
"@
# Write Script
[IO.File]::WriteAllLines('pythonsetupscript.py', $PythonSetupScript)

# Run Setup Script
python ./pythonsetupscript.py

# Delete Python Script
rm pythonsetupscript.py
exit