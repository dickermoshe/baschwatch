# A Couple Of Things To Note:
You must prepare the RPI and your computer before installing.
## Prepare the RPI:
1. The RPI must be powered on.
2. Must contain a blank file named `ssh`
3. Must contain a file named `wpa_supplicant.conf`.  Should contain the following :  
`ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev`  
`country=US`  
`update_config=1`  
`network={`  
  `scan_ssid=1`
`  ssid=">Name of your wireless LAN<"`  
`  psk=">Password for your wireless LAN<"`  
`}`  
## Prepare the PC:
1. Must be connected to the Internet.
2. Must be on same network as RPI
3. Python in not installed / Python is installed through Chocolatey

## Install:
Double click on `install.ps1`

## Run:
Double click on `run.py`
