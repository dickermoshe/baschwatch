#Imports

#System Imports
import os
import sys
import subprocess
import time

#3rd Party
from gpiozero.pins.pigpio import PiGPIOFactory
from gpiozero.input_devices import Button
from gpiozero import MotionSensor
from pythonping import ping
import ipaddress
import win32gui
import win32process
import psutil
import keyboard
from pyvda import get_apps_by_z_order, VirtualDesktop
import click
SELECTEXES = ['chrome.exe','vlc.exe']

class Sensor():
    def __init__(self):
        
        # PIN Numbers
        door_Sensor_PIN = 3
        
        # Get Raspberry IP
        rIP = self.find_RIP() 
        factory = PiGPIOFactory(host=rIP)
        
        # Set SensorType
        self.Sensor = Button(door_Sensor_PIN,pin_factory=factory)
        self.sensorType = 'Door'

        
    
    def wait_for_start(self):
        self.Sensor.wait_for_release()
    
    def wait_for_stop(self):
        self.Sensor.wait_for_press()

    def find_RIP(self):
        try:
            x = ping('raspberrypi')
            rip = x._responses[0].message.source
            rip_version = ipaddress.ip_address(rip).version
            if rip_version != 4:
                raise Exception
            return rip
        except:
            try:
                x = ping('raspberrypi.local')
                rip = x._responses[0].message.source
                rip_version = ipaddress.ip_address(rip).version
                if rip_version != 4:
                    raise Exception
                return rip
            except:
                click.echo("Can't find Raspberry Pi on network")
                sys.exit()

class Desktop():
    def __init__(self):
        t=0 #Just to make sure object is created    
    def getAppsOnDesktop(self):
        apps = get_apps_by_z_order()
        hndle_pid_descrpt_list = []
        for i in apps:
            try:
                pid = win32process.GetWindowThreadProcessId(i.hwnd)[1]
                exe = psutil.Process(pid).exe().split('\\')[-1].lower()
                description = win32gui.GetWindowText(i.hwnd)
                hndle_pid_descrpt_list.append([i.hwnd,pid,exe,description])
            except:
                pass
        return hndle_pid_descrpt_list
        
    def switchDesktop(self,num):
        VirtualDesktop(num).go()
    def currentDesktop(self):
        return VirtualDesktop(current=True).number

class Sound():
    def __init__(self):
        self.SVV = 'svv.exe'
    
    def muteEXEs(self,exes):
        p =[]
        for i in exes:
            p.append(subprocess.Popen([self.SVV,'/Mute',i]))
        for i in p:
            i.wait()
    def unmuteEXEs(self,exes):
        p =[]
        for i in exes:
            p.append(subprocess.Popen([self.SVV,'/Unmute',i]))
        for i in p:
            i.wait()
    def muteAll(self):
        p = subprocess.Popen([self.SVV,'/Mute',"DefaultRenderDevice"])
        p.wait()
    
    def unmuteAll(self):
        p = subprocess.Popen([self.SVV,'/Unmute',"DefaultRenderDevice"])
        p.wait()

class BaschWatch():

    def __init__(self,hideType,muteType,test = False):
        #Check if testing
        self.test = test
        
        #Set CWD
        self.set_cwd_to_script_location()
        
        # Set SoundVolumeView
        self.sv = Sound()

        # Set VirtualDesktopAssistant
        self.vd = Desktop()

        #Set Sensors 
        if not self.test:
            self.sensor = Sensor()

        #Set Types
        self.muteType = muteType
        self.hideType = hideType
        

        self.start()
    def set_cwd_to_script_location(self):
        abspath = os.path.abspath(__file__)
        dname = os.path.dirname(abspath)
        os.chdir(dname)

    def mute(self):
        if self.muteType == 'muteAll':
            self.sv.muteAll()
        elif self.muteType == 'muteOpen':
            self.exes = [i[2] for i in self.vd.getAppsOnDesktop()]
            self.sv.muteEXEs(self.exes)
        elif self.muteType == 'muteSome':
            self.sv.muteEXEs(SELECTEXES)
    def unmute(self):
        if self.muteType == 'muteAll':
            self.sv.unmuteAll()
        elif self.muteType == 'muteOpen':
            self.sv.unmuteEXEs(self.exes)
        elif self.muteType == 'muteSome':
            self.sv.unmuteEXEs(SELECTEXES)
    
    def hide(self):
        if self.hideType == 'minimize':
            keyboard.send('cmd+m')
        elif self.hideType == 'switchDesktop':
            self.current_desktop = self.vd.currentDesktop()
            self.vd.switchDesktop(1)
    def unhide(self):
        if self.hideType == 'switchDesktop':
            self.vd.switchDesktop(self.current_desktop)
    
    def start(self):
        while True:
            if not self.test:
                self.sensor.wait_for_start()
            else:
                input('Press Enter to do hide/mute function...')
            click.echo('Waiting...')
            #Pause
            keyboard.send('space')
            
            #Mute/Hide
            self.mute()
            self.hide()

            #Wait for commands
            click.echo('Press Crtl+Shift+Z to reopen...')
            keyboard.wait('ctrl+shift+z')
            click.echo('Pressed')
            
            #Unmute/Unhide
            self.unmute()
            self.unhide()
            
            #Continue
            #keyboard.send('space')
            
            if not self.test:
                click.echo('Wait to close')
                self.sensor.wait_for_stop()
                time.sleep(1)

def main():
    testKeywords = [False,True]
    muteKeywords = ['muteAll','muteOpen','muteSome']
    hideKeywords = ['minimize','switchDesktop']
    preferredKeywords = [
        ['switchDesktop','muteOpen'],
        ['switchDesktop','muteSome'],
        ['switchDesktop','muteAll']
        ]
    click.echo(f"""
    Enter your preferred action. 
    1. Switch to Desktop #1 and Mute Programs on Current Desktop.
    2. Switch to Desktop #1 and Mute {' & '.join(SELECTEXES)}.
    3. Switch to Desktop #1 and All.
    4. Custom...
    """)
    
    try:
        s = preferredKeywords[int(click.prompt('Please enter a valid selection ',type=click.Choice(['1','2','3','4']),default='1'))-1]
        BaschWatch(*s)
    except IndexError:
        click.echo(f"""
        Enter the mute action:
        1. Mute all Speakers
        2. Mute all Programs on current virtual desktop
        3. Mute only {' & '.join(SELECTEXES)}
        """)
        m = muteKeywords[int(click.prompt('Please enter a valid integer',type=click.Choice(['1','2','3'])))-1]

        click.echo("""
        Enter the hide action:
        1. Minimize all Apps
        2. Go to Desktop #1
        """)
        h = hideKeywords[int(click.prompt('Please enter a valid integer',type=click.Choice(['1','2'])))-1]
        
        click.echo("""
        Enter the hide action:
        1. Use Sensor
        2. Testing Mode
        """)
        t = testKeywords[int(click.prompt('Please enter a valid integer',type=click.Choice(['1','2'])))-1]
        BaschWatch(h,m,test = t)
if __name__ == '__main__':
    main()